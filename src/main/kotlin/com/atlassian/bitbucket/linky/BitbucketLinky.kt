package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.preferences.preferences
import com.atlassian.bitbucket.linky.repository.pullRequestDefaultTargetBranchName
import com.atlassian.bitbucket.linky.selection.LinesSelection
import com.atlassian.bitbucket.linky.selection.compileStringReference
import io.mikael.urlbuilder.UrlBuilder
import io.mikael.urlbuilder.util.UrlParameterMultimap
import java.net.URI
import java.util.*
import kotlin.math.min

typealias Revision = String

fun Revision.short() = this.substring(0, min(length, 8))

interface BitbucketLinky {

    /**
     * @return repository this Linky generates links for
     */
    fun getRepository(): BitbucketRepository

    /**
     * @return base [URI] of the repository
     */
    fun getBaseUri(): URI

    /**
     * Builds a [URI] to the requested commit view.
     */
    fun getCommitViewUri(commitReference: Revision): URI

    /**
     * Builds a [URI] to the commit view with the requested line in the file selected.
     * The file that appears in the commit is the ancestor of the requested file,
     * and the selected line historically corresponds to the requested line.
     */
    fun getCommitViewUri(commitReference: Revision, linkyFile: LinkyFile, lineNumber: Int): URI

    /**
     * @return [URI] to the source view of current branch/commit of the repository
     */
    fun getSourceViewUri(): URI

    /**
     * @return [URI] to the source view with selected lines of the specific file
     */
    fun getSourceViewUri(linkyFile: LinkyFile, linesSelections: List<LinesSelection>): URI

    /**
     * Builds URI for pull request for currently selected branch
     *
     * @return {@link Optional} with [URI] to pull request creation page if current branch name is known,
     * {@link Optional#empty()} otherwise
     */
    fun getPullRequestUri(): Optional<URI>
}

class CloudLinky(private val cloud: BitbucketRepository.Cloud) : BitbucketLinky {
    private val sourceViewRelativePath = "src"

    override fun getRepository(): BitbucketRepository = cloud

    override fun getBaseUri(): URI = cloud.baseUri

    override fun getCommitViewUri(commitReference: Revision): URI =
        cloud.baseUri.resolve("commits/$commitReference")

    override fun getCommitViewUri(commitReference: Revision, linkyFile: LinkyFile, lineNumber: Int): URI =
        UrlBuilder.fromUri(cloud.baseUri.resolve("commits/$commitReference"))
            .withFragment(linkyFile.blameLine(lineNumber)?.let { (file, line) ->
                "L${file}T$line"
            })
            .toUri()

    override fun getSourceViewUri(): URI = cloud.baseUri.resolve(sourceViewRelativePath)

    override fun getSourceViewUri(
        linkyFile: LinkyFile,
        linesSelections: List<LinesSelection>
    ): URI =
        UrlBuilder.fromUri(cloud.baseUri.resolve("$sourceViewRelativePath/${linkyFile.revision}/${linkyFile.relativePath}"))
            .withFragment(linesSelections.compileStringReference(linkyFile.name + "-", ",", ":", ""))
            .toUri()

    override fun getPullRequestUri(): Optional<URI> =
        cloud.repository.currentBranchName.let {
            Optional.ofNullable(it)
                .map { currentBranch ->
                    val parameters = UrlParameterMultimap.newMultimap()
                    parameters.add("source", currentBranch)
                    cloud.repository.preferences().getProperty(pullRequestDefaultTargetBranchName)
                        ?.let { dest -> parameters.add("dest", dest) }

                    UrlBuilder.fromUri(cloud.baseUri.resolve("pull-requests/new"))
                        .withParameters(parameters)
                        .toUri()
                }
        }
}

class ServerLinky(private val server: BitbucketRepository.Server) : BitbucketLinky {
    private val sourceViewRelativePath = "browse"

    override fun getRepository(): BitbucketRepository = server

    override fun getBaseUri(): URI = server.baseUri

    override fun getCommitViewUri(commitReference: Revision): URI =
        server.baseUri.resolve("commits/$commitReference")

    override fun getCommitViewUri(commitReference: Revision, linkyFile: LinkyFile, lineNumber: Int): URI =
        UrlBuilder.fromUri(server.baseUri.resolve("commits/$commitReference"))
            // Bitbucket Server doesn't support line selection in the commit view, so just jump on the file
            .withFragment(linkyFile.blameLine(lineNumber)?.first)
            .toUri()

    override fun getSourceViewUri(): URI = server.baseUri.resolve(sourceViewRelativePath)

    override fun getSourceViewUri(linkyFile: LinkyFile, linesSelections: List<LinesSelection>): URI =
        UrlBuilder.fromUri(server.baseUri.resolve("$sourceViewRelativePath/${linkyFile.relativePath}"))
            .addParameter("at", linkyFile.revision)
            .withFragment(linesSelections.compileStringReference("", ",", "-", ""))
            .toUri()

    override fun getPullRequestUri(): Optional<URI> =
        server.repository.currentBranchName.let {
            Optional.ofNullable(it)
                .map { currentBranch ->
                    val parameters = UrlParameterMultimap.newMultimap()
                    parameters.add("create", null)
                    parameters.add("sourceBranch", currentBranch)
                    server.repository.preferences().getProperty(pullRequestDefaultTargetBranchName)
                        ?.let { dest -> parameters.add("targetBranch", dest) }

                    UrlBuilder.fromUri(server.baseUri.resolve("pull-requests"))
                        .withParameters(parameters)
                        .toUri()
                }
        }
}
