package com.atlassian.bitbucket.linky

import com.atlassian.bitbucket.linky.discovery.BitbucketRepositoriesService
import com.intellij.dvcs.repo.Repository

interface BitbucketLinkyProvider {
    fun getBitbucketLinky(repository: Repository): BitbucketLinky?
}

class DefaultBitbucketLinkyProvider : BitbucketLinkyProvider {

    override fun getBitbucketLinky(repository: Repository): BitbucketLinky? =
        repository.project.getComponent(BitbucketRepositoriesService::class.java)
            ?.getBitbucketRepository(repository)
            ?.let { bitbucketRepo ->
                when (bitbucketRepo) {
                    is BitbucketRepository.Cloud -> CloudLinky(bitbucketRepo)
                    is BitbucketRepository.Server -> ServerLinky(bitbucketRepo)
                }
            }
}
