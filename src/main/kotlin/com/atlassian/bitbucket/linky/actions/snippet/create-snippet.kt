package com.atlassian.bitbucket.linky.actions.snippet

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.actions.Link
import com.atlassian.bitbucket.linky.actions.notificationGroup
import com.atlassian.bitbucket.linky.actions.openUriInBrowser
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons.Actions.SnippetCreate
import com.atlassian.bitbucket.linky.logger
import com.atlassian.bitbucket.linky.preferences.preferences
import com.atlassian.bitbucket.linky.rest.BitbucketRestClientProvider
import com.atlassian.bitbucket.linky.rest.cloud.Access
import com.atlassian.bitbucket.linky.rest.cloud.SnippetFile
import com.atlassian.bitbucket.linky.rest.cloud.oauth.OAuthAccessTokenLateRefreshException
import com.atlassian.bitbucket.linky.rest.cloud.oauth.OAuthException
import com.atlassian.bitbucket.linky.rest.onBitbucketError
import com.atlassian.bitbucket.linky.stats.UsageLog
import com.atlassian.bitbucket.linky.stats.cloudEventAttributes
import com.intellij.notification.NotificationType
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.CommonDataKeys
import com.intellij.openapi.application.ReadAction
import com.intellij.openapi.components.service
import com.intellij.openapi.editor.Editor
import com.intellij.openapi.fileTypes.FileTypeManager
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import com.intellij.openapi.project.DumbAwareAction
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.TextRange
import com.intellij.openapi.vcs.changes.ChangeListManager
import com.intellij.openapi.vfs.VirtualFile
import java.io.IOException

private val log = logger()

class CreateSnippetAction : DumbAwareAction("Create Snippet...", "Create Bitbucket Snippet", SnippetCreate) {

    override fun update(e: AnActionEvent) {
        e.presentation.isEnabledAndVisible = snippetCreationContext(e) != null
    }

    override fun actionPerformed(e: AnActionEvent) {
        val context = snippetCreationContext(e) ?: return
        val project = context.project

        val dialog = CreateSnippetDialog(project, service<BitbucketCloudRegistry>().isStagingDiscovered())
        if (dialog.showAndGet()) {
            val cloud = dialog.getBitbucketInstance()
            val workspace = dialog.workspace()

            val client = project.service<BitbucketRestClientProvider>()
                .bitbucketCloudRestClient(cloud, message("action.create.snippet.authentication.required.label"))

            client.thenApply {
                it?.let { api ->
                    object : Task.Backgroundable(project, "Creating Snippet...") {
                        override fun run(indicator: ProgressIndicator) {
                            api.snippets(workspace)
                                .create(
                                    access = if (dialog.isPrivate()) Access.PRIVATE else Access.PUBLIC,
                                    files = snippetFiles(context),
                                    scm = dialog.getScm(),
                                    title = dialog.getSnippetTitle()
                                )
                                .thenAccept { snippet -> openUriInBrowser(project, Link(snippet.link, "Snippet")) }
                                .thenAccept {
                                    service<UsageLog>().event(
                                        "CreateSnippet",
                                        e.place,
                                        cloudEventAttributes
                                    )
                                }
                                .thenAccept {
                                    when (workspace) {
                                        null -> cloud.preferences().removeProperty(createSnippetWorkspace)
                                        else -> cloud.preferences().setProperty(createSnippetWorkspace, workspace)
                                    }
                                }
                                .onBitbucketError { error ->
                                    when (val cause = error.cause) {
                                        is OAuthAccessTokenLateRefreshException ->
                                            showSnippetCreationError(
                                                project,
                                                cause.message!!
                                            )
                                        is OAuthException -> {
                                            val oAuthCause = cause.cause ?: cause
                                            // TODO bubble up IOException and TimeoutException to top level, leave OAuthException for domain-specific errors only
                                            if (oAuthCause is IOException) {
                                                showIOError(project, oAuthCause)
                                            } else {
                                                showSnippetCreationError(
                                                    project,
                                                    oAuthCause.message ?: message("error.oauth")
                                                )
                                            }
                                        }
                                        is IOException ->
                                            showSnippetCreationError(
                                                project,
                                                cause.message ?: message("error.io")
                                            )
                                        else ->
                                            showSnippetCreationError(
                                                project,
                                                error.message ?: message("error.generic")
                                            ).also {
                                                log.error("Failed to create a snippet", error)
                                            }
                                    }
                                }
                                .join()
                        }
                    }.queue()
                }
            }
        }
    }


    private fun snippetFiles(context: SnippetCreationContext): List<SnippetFile> =
        ReadAction.compute<List<SnippetFile>, Exception> {
            with(context) {
                when {
                    editor != null -> {
                        val selectedText = selectedTextSnippet(editor)
                        listOf(SnippetFile(file?.name ?: "file") {
                            selectedText.byteInputStream()
                        }
                        )
                    }
                    files.isNotEmpty() -> files
                        .flatMap { f -> f.snippetHunks(project, "") }
                        .map { (name, file) ->
                            SnippetFile(
                                name,
                                file::getInputStream
                            )
                        }
                    file != null -> listOf(
                        SnippetFile(
                            file.name,
                            file::getInputStream
                        )
                    )
                    else -> listOf()
                }
            }
        }

    private fun snippetCreationContext(e: AnActionEvent): SnippetCreationContext? {
        val context = e.dataContext

        val project = CommonDataKeys.PROJECT.getData(context)
        val editor = CommonDataKeys.EDITOR.getData(context)
        val file = CommonDataKeys.VIRTUAL_FILE.getData(context)
        val files = CommonDataKeys.VIRTUAL_FILE_ARRAY.getData(context) ?: arrayOf()

        return if (project == null ||
            (editor == null && file == null && files.isEmpty()) ||
            (editor != null && editor.document.textLength == 0)
        ) {
            null
        } else {
            SnippetCreationContext(project, editor, file, files.toList())
        }
    }

    private fun selectedTextSnippet(editor: Editor): String {
        val hunks = with(editor.selectionModel) {
            blockSelectionStarts
                .zip(blockSelectionEnds)
                .filter { (start, end) -> start != end }
                .map { (start, end) -> editor.document.getText(TextRange.create(start, end)) }
        }
        return when {
            hunks.isEmpty() -> editor.document.text
            else -> hunks.joinToString("\n\n...\n\n")
        }
    }

    private fun VirtualFile.snippetHunks(project: Project, namePrefix: String): List<Pair<String, VirtualFile>> =
        if (isDirectory) {
            children
                .filterNot { isFileIgnored(project, it) }
                .flatMap { it.snippetHunks(project, "$namePrefix$name/") }
        } else {
            listOf("$namePrefix$name" to this)
        }

    private fun isFileIgnored(project: Project, file: VirtualFile) =
        ChangeListManager.getInstance(project).isIgnoredFile(file) ||
                FileTypeManager.getInstance().isFileIgnored(file)


    private data class SnippetCreationContext(
        val project: Project,
        val editor: Editor?,
        val file: VirtualFile?,
        val files: List<VirtualFile>
    )

    private fun showIOError(project: Project, e: IOException) {
        showSnippetCreationError(project, e.message ?: message("error.io"))
    }

    private fun showSnippetCreationError(project: Project, text: String) {
        notificationGroup.createNotification(
            message("action.create.snippet.error.notification.title"),
            message("action.create.snippet.error.notification.text", text),
            NotificationType.ERROR,
            null
        ).setIcon(BitbucketLinkyIcons.Bitbucket)
            .setImportant(true)
            .notify(project)
    }
}
