package com.atlassian.bitbucket.linky.blame

import com.atlassian.bitbucket.linky.LinkyFile
import com.atlassian.bitbucket.linky.RelativePath
import com.atlassian.bitbucket.linky.logger
import com.intellij.dvcs.repo.Repository
import org.zmlx.hg4idea.execution.HgCommandExecutor
import org.zmlx.hg4idea.repo.HgRepository


private val log = logger()

class HgLineBlamerProvider : LineBlamerProvider {
    override fun getLineBlamer(repository: Repository): LineBlamer? = when (repository) {
        is HgRepository -> HgLineBlamer
        else -> null
    }
}

private object HgLineBlamer : LineBlamer {
    private val linePattern = "\\s*[0-9a-fA-F]+\\s+(.+):\\s*([0-9]+):\\s.*".toPattern()

    override fun blameLine(linkyFile: LinkyFile, lineNumber: Int) =
        runAnnotateCommand(linkyFile)
            // index starts from 0, line number - from 1
            .getOrNull(lineNumber - 1)
            ?.let { parseReferenceFromBlameOutput(it) }

    private fun runAnnotateCommand(linkyFile: LinkyFile): List<String> {
        // hg annotate -f -c -l -w -r revision svnImportScript.sh
        val arguments = listOf("-f", "-c", "-l", "-w", "-r", linkyFile.revision, linkyFile.relativePath)
        log.debug("Running command 'hg annotate $arguments'")

        val result = HgCommandExecutor(linkyFile.repository.project)
            .executeInCurrentThread(linkyFile.repository.root, "annotate", arguments)

        return result?.outputLines ?: emptyList()
    }

    private fun parseReferenceFromBlameOutput(outputLine: String): Pair<RelativePath, Int>? {
        val matcher = linePattern.matcher(outputLine)
        if (matcher.matches()) {
            val lineNumber = matcher.group(2).toIntOrNull()

            if (lineNumber == null) {
                log.error("Failed to parse line number from hg annotate output line '$outputLine'")
                return null
            }

            return Pair(matcher.group(1), lineNumber)
        }

        log.error("Hg output doesn't match expected pattern: '$outputLine'")
        return null
    }
}
