package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.BitbucketRepository
import com.atlassian.bitbucket.linky.BitbucketRepository.Cloud
import com.atlassian.bitbucket.linky.BitbucketRepository.Server
import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
import com.atlassian.bitbucket.linky.preferences.preferences
import com.atlassian.bitbucket.linky.repository.alwaysLinkToSelectedRemote
import com.atlassian.bitbucket.linky.repository.getOrderedRemoteUrls
import com.atlassian.bitbucket.linky.repository.traverseRepositories
import com.google.common.util.concurrent.ThreadFactoryBuilder
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.ProjectComponent
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.vcs.ProjectLevelVcsManager
import com.intellij.openapi.vcs.VcsListener
import java.time.Duration
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit

interface BitbucketRepositoriesService {
    fun getBitbucketRepositories(repository: Repository): Map<RemoteUrl, BitbucketRepository>
    fun getBitbucketRepository(repository: Repository): BitbucketRepository?

    fun configurationChanged()
}

class DefaultBitbucketRepositoriesService(
    private val project: Project
) : ProjectComponent, BitbucketRepositoriesService, VcsListener {

    private val discoveryDelay = Duration.ofMinutes(10)
    private val discoveryExecutor = Executors.newSingleThreadScheduledExecutor(
        ThreadFactoryBuilder()
            .setNameFormat("bitbucket-reference-plugin-remote-discoverer-%d")
            .setDaemon(true)
            .build()
    )
    private val discoveryTask: () -> Unit = {
        project.traverseRepositories()
            .forEach { RepositoryAnalyzer.analyzeRepository(it) }
    }

    private var discoveryTaskFuture: ScheduledFuture<*>? = null

    override fun initComponent() {
        project.messageBus.connect()
            .subscribe(ProjectLevelVcsManager.VCS_CONFIGURATION_CHANGED, this)
    }

    override fun getBitbucketRepositories(repository: Repository): Map<RemoteUrl, BitbucketRepository> =
        repository.remoteUrlCandidates()
            .mapNotNull { remoteUrl -> getBitbucketRepository(repository, remoteUrl)?.let { remoteUrl to it } }
            .toMap()

    override fun getBitbucketRepository(repository: Repository): BitbucketRepository? =
        repository.remoteUrlCandidates()
            .mapNotNull { remoteUrl -> getBitbucketRepository(repository, remoteUrl) }
            .firstOrNull()

    override fun configurationChanged() {
        discoveryTaskFuture?.cancel(true)
        discoveryTaskFuture = discoveryExecutor.scheduleAtFixedRate(
            discoveryTask, 0, discoveryDelay.toMinutes(), TimeUnit.MINUTES
        )
    }

    override fun directoryMappingChanged() {
        configurationChanged()
    }

    private fun getBitbucketRepository(repository: Repository, remoteUrl: RemoteUrl): BitbucketRepository? {
        if (remoteUrl.matchesCloudPattern()) {
            val (workspace, slug) = remoteUrl.cloudWorkspaceAndSlug()
            val cloudRegistry = service<BitbucketCloudRegistry>()
            cloudRegistry.lookup(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port)
                ?.let { return Cloud(repository, remoteUrl, it, workspace, slug) }
        }
        if (remoteUrl.matchesServerPattern()) {
            val (appPath, project, slug) = remoteUrl.serverPathProjectAndSlug()
            val serverRegistry = service<BitbucketServerRegistry>()
            serverRegistry.lookup(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath)
                ?.let { return Server(repository, remoteUrl, it, project, slug) }
        }
        return null
    }

    private fun Repository.remoteUrlCandidates(): List<RemoteUrl> =
        // if specific remote URL is defined in properties, use it
        preferences().getProperty(alwaysLinkToSelectedRemote)
            // fallback to remote URL selected based on the repository
            ?.let { serializedRemote -> RemoteUrl.parse(serializedRemote) }
            ?.let { listOf(it) }
        // same for the case when no specific remote URL is defined
            ?: getOrderedRemoteUrls()
}
