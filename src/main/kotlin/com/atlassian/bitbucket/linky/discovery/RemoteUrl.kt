package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.UriScheme.HTTP
import com.atlassian.bitbucket.linky.UriScheme.HTTPS
import java.net.URI
import java.util.regex.Pattern

data class RemoteUrl(val scheme: UriScheme, val hostname: String, val port: Int, val path: String) {
    init {
        if (!path.startsWith("/")) {
            throw IllegalArgumentException("Path '$path' should start with a slash")
        }
        if (path.length > 1 && path.endsWith("/")) {
            throw IllegalArgumentException("Path '$path' should not end with a slash")
        }
    }

    fun serialize() = URI(scheme.presentation, null, hostname, port, path, null, null).toString()

    companion object {
        fun parse(string: String): RemoteUrl? {
            val url = URI.create(string)
            return UriScheme.forName(url.scheme)
                ?.let { RemoteUrl(it, url.host, url.port, url.path) }
        }
    }
}

private val cloudPathPattern: Pattern = "/([^/]+)/([^/]+)".toPattern()
private const val serverAppPathPattern = "(/.+)?"
private const val serverFullSlugPattern = "([^/]+)/([^/]+)"
private val serverHttpPathPattern = "$serverAppPathPattern/scm/$serverFullSlugPattern".toPattern()
private val serverSshPathPattern = "$serverAppPathPattern/$serverFullSlugPattern".toPattern()


fun RemoteUrl.matchesCloudPattern() = cloudPathPattern.matcher(path).matches()
fun RemoteUrl.cloudWorkspaceAndSlug(): Pair<String, String> {
    val matcher = cloudPathPattern.matcher(path)
    if (matcher.matches()) {
        val workspaceId: String = matcher.group(1)
        val slug: String = matcher.group(2)
        return workspaceId to slug
    }
    throw IllegalArgumentException("Path doesn't match Bitbucket Cloud pattern")
}

fun RemoteUrl.matchesServerPattern() = serverPathPattern.matcher(path).matches()
fun RemoteUrl.serverPathProjectAndSlug(): Triple<String, String, String> {
    val matcher = serverPathPattern.matcher(path)
    if (matcher.matches()) {
        val appPath: String = matcher.group(1) ?: "/"
        val projectKey: String = matcher.group(2)
        val slug: String = matcher.group(3)
        return Triple(appPath, projectKey, slug)
    }
    throw IllegalArgumentException("Path doesn't match Bitbucket Server pattern")
}

private val RemoteUrl.serverPathPattern
    get() = when (scheme) {
        HTTP, HTTPS -> serverHttpPathPattern
        else -> serverSshPathPattern
    }
