package com.atlassian.bitbucket.linky.discovery

import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
import com.atlassian.bitbucket.linky.hosting.BitbucketServerRegistry
import com.atlassian.bitbucket.linky.repository.getRemoteUrls
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.service

object RepositoryAnalyzer {
    fun analyzeRepository(repository: Repository) {
        repository.getRemoteUrls().forEach {
            RemoteUrlAnalyzer.analyzeRemoteUrl(it)
        }
    }
}

object RemoteUrlAnalyzer {
    private val cloudRegistry = service<BitbucketCloudRegistry>()
    private val serverRegistry = service<BitbucketServerRegistry>()

    fun analyzeRemoteUrl(remoteUrl: RemoteUrl) {
        knownCloud(remoteUrl)?.let { return }
        knownServer(remoteUrl)?.let { return }

        if (remoteUrl.matchesCloudPattern()) {
            BitbucketCloudProbe.probeBitbucketCloud(remoteUrl.hostname)
                ?.let { cloud ->
                    cloudRegistry.add(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, cloud)
                    return
                }
        }
        if (remoteUrl.matchesServerPattern()) {
            val (appPath, _, _) = remoteUrl.serverPathProjectAndSlug()
            BitbucketServerProbe.probeBitbucketServer(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath)
                .thenApply {
                    it?.let { server ->
                        serverRegistry.add(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath, server)
                    }
                }
        }
    }

    private fun knownCloud(remoteUrl: RemoteUrl): BitbucketCloud? =
        if (remoteUrl.matchesCloudPattern()) {
            cloudRegistry.lookup(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port)
        } else null

    private fun knownServer(remoteUrl: RemoteUrl): BitbucketServer? =
        if (remoteUrl.matchesServerPattern()) {
            val (appPath, _, _) = remoteUrl.serverPathProjectAndSlug()
            serverRegistry.lookup(remoteUrl.scheme, remoteUrl.hostname, remoteUrl.port, appPath)
        } else null

}
