@file:JvmName("HostingUtils")

package com.atlassian.bitbucket.linky.hosting

import com.atlassian.bitbucket.linky.UriScheme
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import java.util.*

val BitbucketServer.uuid: UUID
    get() = UUID.nameUUIDFromBytes(baseUrl.toString().toByteArray())

interface BitbucketCloudRegistry {
    fun add(scheme: UriScheme, hostname: String, port: Int, cloud: BitbucketCloud)
    fun lookup(scheme: UriScheme, hostname: String, port: Int): BitbucketCloud?
    fun isStagingDiscovered(): Boolean
}

interface BitbucketServerRegistry {
    fun add(scheme: UriScheme, hostname: String, port: Int, path: String, server: BitbucketServer)
    fun lookup(scheme: UriScheme, hostname: String, port: Int, path: String): BitbucketServer?
}
