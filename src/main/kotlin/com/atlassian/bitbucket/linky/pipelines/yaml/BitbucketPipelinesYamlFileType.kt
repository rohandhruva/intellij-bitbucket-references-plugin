package com.atlassian.bitbucket.linky.pipelines.yaml

import com.atlassian.bitbucket.linky.icons.BitbucketLinkyIcons
import com.intellij.openapi.fileTypes.LanguageFileType
import org.jetbrains.yaml.YAMLLanguage

object BitbucketPipelinesYamlFileType : LanguageFileType(YAMLLanguage.INSTANCE) {

    val filenames = arrayOf("bitbucket-pipelines.yml", "bitbucket-pipelines.yaml")

    override fun getIcon() = BitbucketLinkyIcons.Bitbucket

    override fun getName() = "BitbucketPipelines"

    override fun getDefaultExtension() = "yml"

    override fun getDescription() = "Bitbucket Pipelines configuration file"
}
