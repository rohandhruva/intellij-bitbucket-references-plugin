package com.atlassian.bitbucket.linky.pipelines.yaml.inspection

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.cacheDefinitionPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.pattern.dockerOptionPattern
import com.atlassian.bitbucket.linky.pipelines.yaml.reference.CacheDefinitionReference
import com.atlassian.bitbucket.linky.pipelines.yaml.reference.ServiceDefinitionReference
import com.atlassian.bitbucket.linky.pipelines.yaml.resolve.resolveOptions
import com.intellij.codeInspection.LocalInspectionTool
import com.intellij.codeInspection.ProblemHighlightType
import com.intellij.codeInspection.ProblemsHolder
import com.intellij.psi.PsiElementVisitor
import org.jetbrains.yaml.psi.*

private const val DOCKER_NAME = "docker"

class DockerUsageInspection : LocalInspectionTool() {

    override fun buildVisitor(
        holder: ProblemsHolder,
        isOnTheFly: Boolean
    ): PsiElementVisitor = object : YamlPsiElementVisitor() {
        override fun visitKeyValue(keyValue: YAMLKeyValue) {
            if (keyValue.keyText == DOCKER_NAME) {
                if (dockerOptionPattern.accepts(keyValue) && keyValue.valueText.toBoolean()) {
                    keyValue.key?.let {
                        holder.registerProblem(
                            it,
                            message("inspections.docker.usage.step.service.preferred.problem.text"),
                            ProblemHighlightType.WEAK_WARNING
                        )
                    }
                } else if (cacheDefinitionPattern.accepts(keyValue)) {
                    keyValue.key?.let {
                        holder.registerProblem(
                            it,
                            message("inspections.docker.usage.cache.name.reserved.problem.text", DOCKER_NAME),
                            ProblemHighlightType.GENERIC_ERROR_OR_WARNING
                        )
                    }
                }
            }
        }

        override fun visitScalar(scalar: YAMLScalar) {
            if (scalar.textValue == DOCKER_NAME) {
                when (scalar.reference) {
                    is CacheDefinitionReference -> inspectDockerCacheReference(scalar)
                    is ServiceDefinitionReference -> inspectDockerServiceReference(scalar)
                }
            }
        }

        fun inspectDockerCacheReference(scalar: YAMLScalar) {
            if (resolveOptions(scalar.containingFile).dockerEnabled) return

            val stepMapping = scalar.parent?.parent?.parent?.parent as? YAMLMapping ?: return
            val servicesKeyValue = stepMapping.getKeyValueByKey("services") ?: return
            val servicesSequence = servicesKeyValue.children
                .filterIsInstance<YAMLSequence>()
                .firstOrNull() ?: return
            val dockerServiceDefined = servicesSequence.items.mapNotNull { item ->
                item.children
                    .filterIsInstance<YAMLScalar>()
                    .firstOrNull { it.textValue == DOCKER_NAME }
            }.isNotEmpty()

            if (dockerServiceDefined) return

            // TODO quick fix(es) to set global option and/or to add docker service to the step
            holder.registerProblem(
                scalar,
                message("inspections.docker.usage.cache.docker.not.enabled.problem.text"),
                ProblemHighlightType.GENERIC_ERROR_OR_WARNING
            )
        }

        fun inspectDockerServiceReference(scalar: YAMLScalar) {
            if (resolveOptions(scalar.containingFile).dockerEnabled) {
                holder.registerProblem(
                    scalar,
                    message("inspections.docker.usage.redundant.service.problem.text"),
                    ProblemHighlightType.LIKE_UNUSED_SYMBOL
                )
            }
        }
    }
}
