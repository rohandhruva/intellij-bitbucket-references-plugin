package com.atlassian.bitbucket.linky.pipelines.yaml.pattern

import com.intellij.patterns.ElementPattern
import com.intellij.patterns.PlatformPatterns.psiElement
import org.jetbrains.yaml.psi.YAMLDocument
import org.jetbrains.yaml.psi.YAMLKeyValue
import org.jetbrains.yaml.psi.YAMLSequence

val cacheDefinitionsPattern: ElementPattern<YAMLKeyValue> =
    psiElement(YAMLKeyValue::class.java)
        .and(inPipelinesFilePattern)
        .and(yamlKeyValuePattern("caches"))
        .withSuperParent(2, yamlKeyValuePattern("definitions"))
        .withSuperParent(4, YAMLDocument::class.java)

val cacheDefinitionPattern: ElementPattern<YAMLKeyValue> =
    psiElement(YAMLKeyValue::class.java)
        .withSuperParent(2, cacheDefinitionsPattern)

val cacheReferencesPattern: ElementPattern<YAMLSequence> =
    psiElement(YAMLSequence::class.java)
        .withParent(
            psiElement(YAMLKeyValue::class.java)
                .and(yamlKeyValuePattern("caches"))
                .withSuperParent(2, yamlKeyValuePattern("step"))
        )
