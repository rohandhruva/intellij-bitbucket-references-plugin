package com.atlassian.bitbucket.linky.pipelines.yaml.schema

import com.atlassian.bitbucket.linky.pipelines.yaml.BitbucketPipelinesYamlFileType
import com.intellij.openapi.project.Project
import com.intellij.openapi.vfs.VfsUtil
import com.intellij.openapi.vfs.VirtualFile
import com.jetbrains.jsonSchema.extension.JsonSchemaFileProvider
import com.jetbrains.jsonSchema.extension.JsonSchemaProviderFactory
import com.jetbrains.jsonSchema.extension.SchemaType

object PipelinesJsonSchemaProviderFactory : JsonSchemaProviderFactory {
    override fun getProviders(project: Project) = listOf(PipelinesJsonSchemaProvider)
}

object PipelinesJsonSchemaProvider : JsonSchemaFileProvider {

    override fun getName() = "Bitbucket Pipelines configuration"

    override fun isAvailable(file: VirtualFile) =
        file.fileType == BitbucketPipelinesYamlFileType

    override fun getSchemaFile() =
        VfsUtil.findFileByURL(javaClass.getResource(SCHEMA_FILE_PATH))

    override fun getSchemaType() = SchemaType.embeddedSchema
}

const val SCHEMA_FILE_PATH = "/schemas/bitbucket-pipelines.schema.json"
