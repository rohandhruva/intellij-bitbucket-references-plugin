package com.atlassian.bitbucket.linky.pipelines.yaml.template

import com.intellij.codeInsight.template.impl.DefaultLiveTemplatesProvider

class PipelinesTemplatesProvider : DefaultLiveTemplatesProvider {

    override fun getDefaultLiveTemplateFiles(): Array<String> = arrayOf("liveTemplates/BitbucketPipelines")

    override fun getHiddenLiveTemplateFiles(): Array<String> = emptyArray()
}
