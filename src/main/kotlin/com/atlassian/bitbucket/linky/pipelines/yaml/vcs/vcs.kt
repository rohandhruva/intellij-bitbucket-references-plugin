package com.atlassian.bitbucket.linky.pipelines.yaml.vcs

import com.intellij.dvcs.repo.VcsRepositoryManager
import com.intellij.psi.PsiFile

fun repositoryForFile(file: PsiFile) =
    file.project.getComponent(VcsRepositoryManager::class.java)
        ?.getRepositoryForFile(file.virtualFile)
