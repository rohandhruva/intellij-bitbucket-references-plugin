package com.atlassian.bitbucket.linky.preferences

import com.atlassian.bitbucket.linky.SETTINGS_STORAGE_FILE
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.openapi.util.text.StringUtil.escapeXmlEntities
import com.intellij.openapi.util.text.StringUtil.unescapeXmlEntities
import org.jdom.Element


fun BitbucketCloud.preferences() = object : Preferences {
    private val preferencesState = service<BitbucketInstancePreferencesState>()

    override fun getProperty(key: String, defaultValue: String?): String? =
        preferencesState.properties.getOrDefault(name to key, defaultValue)

    override fun setProperty(key: String, value: String) {
        preferencesState.properties[name to key] = value
    }

    override fun removeProperty(key: String) {
        preferencesState.properties.remove(name to key)
    }
}

@State(name = "BitbucketInstancePreferences", storages = [Storage(SETTINGS_STORAGE_FILE)])
class BitbucketInstancePreferencesState : PersistentStateComponent<Element> {

    internal val properties = mutableMapOf<Pair<String, String>, String>()

    override fun getState() =
        Element("state").apply {
            properties.forEach { (keyPair, propertyValue) ->
                val (bitbucketId, propertyKey) = keyPair
                addContent(Element("property").apply {
                    setAttribute("instance", bitbucketId)
                    setAttribute("key", propertyKey)
                    setAttribute("value", escapeXmlEntities(propertyValue))
                })
            }
        }

    override fun loadState(element: Element) {
        properties.clear()
        element.getChildren("property")
            .forEach {
                val bitbucketId = it.getAttributeValue("instance")
                val key = it.getAttributeValue("key")
                val value = it.getAttributeValue("value")
                properties[bitbucketId to key] = unescapeXmlEntities(value)
            }
    }
}
