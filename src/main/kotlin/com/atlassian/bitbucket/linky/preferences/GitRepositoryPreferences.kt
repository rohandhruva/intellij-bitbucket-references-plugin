package com.atlassian.bitbucket.linky.preferences

import com.atlassian.bitbucket.linky.logger
import com.google.common.cache.CacheBuilder
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import git4idea.commands.Git
import git4idea.repo.GitRepository
import java.time.Duration

class GitRepositoryPreferences : RepositoryPreferences {
    private val cache =
        CacheBuilder.newBuilder()
            .expireAfterAccess(Duration.ofDays(7))
            .build<String, GitConfigHolder>()

    override fun preferences(repository: Repository): Preferences? =
        when (repository) {
            is GitRepository -> GitConfigPreferences(
                repository,
                // Cache by a constant path under .git directory
                cache.get(repository.repositoryFiles.shallowFile.absolutePath) {
                    GitConfigHolder(repository)
                }
            )
            else -> null
        }
}

private val log = logger()

private class GitConfigHolder(repository: GitRepository) {
    private val prefix = "bitbucket-linky."
    private val properties: MutableMap<String, String>

    init {
        val result = Git.getInstance().config(repository, "--local", "--list")
        if (result.success()) {
            properties = result.output
                .filter { it.startsWith(prefix) }
                .map { it.removePrefix(prefix) }
                .mapNotNull { it.parseKeyValue() }
                .toMap(mutableMapOf())
        } else {
            throw IllegalStateException("Failed to load Git repository properties: ${result.errorOutputAsJoinedString}")
        }
    }

    fun getProperty(key: String, defaultValue: String?): String? =
        properties.getOrDefault(key, defaultValue)

    fun setProperty(repository: GitRepository, key: String, value: String) {
        properties[key] = value
        updateProperties(repository, "$prefix$key", value)
    }

    fun removeProperty(repository: GitRepository, key: String) {
        properties.remove(key)
        updateProperties(repository, "--unset", "$prefix$key")
    }

    private fun updateProperties(repository: GitRepository, vararg params: String) {
        object : Task.Backgroundable(repository.project, "Updating repository properties...", false) {
            override fun run(indicator: ProgressIndicator) {
                val result = Git.getInstance().config(repository, "--local", *params)
                if (!result.success()) {
                    log.error("Failed to update Git repository properties with parameters '${params.contentToString()}': ${result.errorOutputAsJoinedString}")
                }
            }
        }.queue()
    }
}

private class GitConfigPreferences(
    private val repository: GitRepository,
    private val configHolder: GitConfigHolder
) : Preferences {

    override fun getProperty(key: String, defaultValue: String?): String? = configHolder.getProperty(key, defaultValue)

    override fun setProperty(key: String, value: String) {
        configHolder.setProperty(repository, key, value)
    }

    override fun removeProperty(key: String) {
        configHolder.removeProperty(repository, key)
    }
}
