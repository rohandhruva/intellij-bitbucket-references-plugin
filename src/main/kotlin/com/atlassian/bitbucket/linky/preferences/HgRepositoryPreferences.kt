package com.atlassian.bitbucket.linky.preferences

import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.progress.ProgressIndicator
import com.intellij.openapi.progress.Task
import com.intellij.openapi.util.io.FileUtil
import com.intellij.openapi.vfs.VfsUtilCore
import org.zmlx.hg4idea.repo.HgRepository
import java.io.File

class HgRepositoryPreferences : RepositoryPreferences {
    override fun preferences(repository: Repository): Preferences? =
        when (repository) {
            is HgRepository -> HgConfigPreferences(repository)
            else -> null
        }
}

private class HgConfigPreferences(private val repository: HgRepository) : Preferences {
    private val sectionName = "bitbucket-linky"
    private val fileName = "$sectionName.rc"

    override fun getProperty(key: String, defaultValue: String?): String? =
        repository.repositoryConfig.getNamedConfig(sectionName, key) ?: defaultValue

    override fun setProperty(key: String, value: String) {
        updateProperties { it.toMutableMap().apply { put(key, value) } }
    }

    override fun removeProperty(key: String) {
        updateProperties { it.toMutableMap().apply { remove(key) } }
    }

    private fun updateProperties(update: (Map<String, String>) -> Map<String, String>) {
        object : Task.Backgroundable(repository.project, "Updating repository properties...", false) {
            override fun run(indicator: ProgressIndicator) {
                val linkyFile = getLinkyPropertiesFile()
                val currentProperties = linkyFile.readLines()
                    .mapNotNull { it.parseKeyValue() }
                    .toMap()
                val newProperties = update(currentProperties)
                FileUtil.writeToFile(
                    linkyFile,
                    newProperties.entries.joinToString(
                        prefix = "[$sectionName]\n",
                        separator = "\n",
                        postfix = "\n",
                        transform = { (key, value) -> "$key = $value" }
                    )
                )
                repository.updateConfig()
            }
        }.queue()
    }

    private fun getLinkyPropertiesFile(): File {
        val hgDir = VfsUtilCore.virtualToIoFile(repository.hgDir)
        val linkyFile = File(hgDir, fileName)
        if (!linkyFile.exists()) {
            FileUtil.appendToFile(linkyFile, "[$sectionName]\n")
            val hgrc = File(hgDir, "hgrc")
            FileUtil.appendToFile(hgrc, "\n%include $fileName\n")
        }
        return linkyFile
    }
}
