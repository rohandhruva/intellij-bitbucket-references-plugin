package com.atlassian.bitbucket.linky.preferences

import com.atlassian.bitbucket.linky.repository.traverseRepositories
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.components.service
import com.intellij.openapi.project.Project
import com.intellij.openapi.startup.StartupActivity
import com.intellij.openapi.util.text.StringUtil
import org.jdom.Element

class RepositoryPropertiesMigration : StartupActivity {

    override fun runActivity(project: Project) {
        val legacyProperties = project.service<RepositoryPreferencesState>().properties
        if (legacyProperties.isEmpty()) {
            return
        }

        val repositories = project.traverseRepositories()
        legacyProperties.forEach { (keyPair, value) ->
            val (repoRelativePath, key) = keyPair

            fun Repository.migrateProperty() {
                preferences().setProperty(key.replace('_', '-'), value)
            }

            if (repoRelativePath == "/") {
                if (repositories.size == 1) {
                    repositories.first().migrateProperty()
                }
            } else {
                repositories.firstOrNull { it.root.path.endsWith(repoRelativePath) }?.migrateProperty()
            }
        }
        legacyProperties.clear()
    }
}

@State(name = "BitbucketRepositoryProperties", storages = arrayOf(Storage("bitbucket-repo-properties.xml")))
class RepositoryPreferencesState : PersistentStateComponent<Element> {

    internal val properties = mutableMapOf<Pair<String, String>, String>()

    override fun getState(): Element {
        return Element("BitbucketRepositoryProperties").apply {
            // Properties are stored under repository element, so regroup them here TODO refactor storage?
            properties.keys.groupBy { (relativePath, _) -> relativePath }
                .forEach { (relativePath, keys) ->
                    addContent(Element("repository").apply {
                        setAttribute("relativeRootPath", relativePath)
                        addContent(Element("properties").apply {
                            keys.forEach { key ->
                                val value = properties[key]
                                if (value != null) {
                                    addContent(Element("property").apply {
                                        setAttribute("name", key.second)
                                        addContent(StringUtil.escapeXmlEntities(value))
                                    })
                                }
                            }
                        })
                    })
                }
        }
    }

    override fun loadState(element: Element) {
        properties.clear()
        element.getChildren("repository")
            .forEach { repoElement ->
                val relativePath = repoElement.getAttributeValue("relativeRootPath")
                repoElement.getChild("properties")
                    .getChildren("property")
                    .forEach {
                        val propertyName = it.getAttributeValue("name")
                        val propertyValue = it.value
                        properties[relativePath to propertyName] = StringUtil.unescapeXmlEntities(propertyValue)
                    }
            }
    }
}
