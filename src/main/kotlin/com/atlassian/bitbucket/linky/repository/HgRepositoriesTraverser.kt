package com.atlassian.bitbucket.linky.repository

import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.project.Project
import org.zmlx.hg4idea.util.HgUtil

class HgRepositoriesTraverser : RepositoriesTraverser {
    override fun traverseRepositories(project: Project): Collection<Repository> =
        HgUtil.getRepositoryManager(project).repositories
}
