package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.intellij.dvcs.repo.Repository
import org.zmlx.hg4idea.repo.HgRepository

class HgRepositoryExtensionsProvider : RepositoryExtensionsProvider {
    override fun getExtensions(repository: Repository): RepositoryExtensions? =
        when (repository) {
            is HgRepository -> HgRepositoryExtensions(repository)
            else -> null
        }
}

private class HgRepositoryExtensions(private val repository: HgRepository) : RepositoryExtensions {
    override fun getCurrentRemoteUrl(): RemoteUrl? =
        repository.repositoryConfig
            .defaultPushPath
            ?.let { HgRemoteUrlParser.parseRemoteUrl(it) }

    override fun getRemoteUrls(): List<RemoteUrl> =
        repository
            .repositoryConfig
            .paths
            .distinct()
            .mapNotNull { HgRemoteUrlParser.parseRemoteUrl(it) }

    override fun hasRevisionBeenPushed(revision: Revision): Boolean =
        repository.project
            .getComponent(HgRevisionPushStatusReporter::class.java)
            .hasRevisionBeenPushed(repository, revision)
}
