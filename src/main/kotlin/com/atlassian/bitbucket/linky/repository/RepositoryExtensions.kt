package com.atlassian.bitbucket.linky.repository

import com.atlassian.bitbucket.linky.Revision
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.intellij.dvcs.repo.Repository
import com.intellij.openapi.extensions.ExtensionPointName

interface RepositoryExtensionsProvider {
    fun getExtensions(repository: Repository): RepositoryExtensions?

    companion object {
        val EP_NAME =
            ExtensionPointName.create<RepositoryExtensionsProvider>("com.atlassian.bitbucket.linky.repositoryExtensions")
    }
}

interface RepositoryExtensions {
    fun getCurrentRemoteUrl(): RemoteUrl?
    fun getRemoteUrls(): List<RemoteUrl>
    fun hasRevisionBeenPushed(revision: Revision): Boolean
}

val Repository.extensions
    get() = RepositoryExtensionsProvider.EP_NAME.extensions
        .mapNotNull { it.getExtensions(this) }
        .firstOrNull()

fun Repository.getCurrentRemoteUrl(): RemoteUrl? = extensions?.getCurrentRemoteUrl()
fun Repository.getRemoteUrls(): List<RemoteUrl> = extensions?.getRemoteUrls() ?: emptyList()
fun Repository.hasRevisionBeenPushed(revision: Revision) = extensions?.hasRevisionBeenPushed(revision) ?: true
fun Repository.getOrderedRemoteUrls(): List<RemoteUrl> =
    getCurrentRemoteUrl()
        ?.let { current -> listOf(current) + getRemoteUrls().filterNot { it == current } }
        ?: getRemoteUrls()
