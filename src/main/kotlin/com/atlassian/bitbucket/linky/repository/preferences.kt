package com.atlassian.bitbucket.linky.repository

const val pullRequestDefaultTargetBranchName = "pull-request-default-target-branch-name"
const val alwaysLinkToSelectedRemote = "always-link-to-remote"
