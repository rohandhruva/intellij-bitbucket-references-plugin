package com.atlassian.bitbucket.linky.rest.auth

import java.time.Instant

inline class RefreshToken(val value: String)

class AccessToken(val value: String, val expiresAt: Instant) {
    fun isExpired() = Instant.now().isAfter(expiresAt)
}

sealed class OAuthTokens {
    abstract val refreshToken: RefreshToken

    data class RefreshTokenOnly(
        override val refreshToken: RefreshToken
    ) : OAuthTokens()

    data class RefreshAndAccessTokens(
        override val refreshToken: RefreshToken,
        val accessToken: AccessToken
    ) : OAuthTokens()
}
