package com.atlassian.bitbucket.linky.rest.auth

import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.github.kittinunf.fuel.core.FoldableRequestInterceptor
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.RequestTransformer

private const val FEATURE = "NO_AUTO_AUTHENTICATION"

abstract class RequestAuthenticationInterceptorBase(private vararg val hostnames: String) : FoldableRequestInterceptor {
    constructor(cloud: BitbucketCloud) : this(cloud.hostname, cloud.apiHostname)
    constructor(server: BitbucketServer) : this(server.hostname)

    override fun invoke(next: RequestTransformer): RequestTransformer =
        { request ->
            when {
                request.header(Headers.AUTHORIZATION).isNotEmpty() ->
                    next(request)
                request.url.host !in hostnames ->
                    next(request)
                request.enabledFeatures.getOrDefault(FEATURE, null) != null ->
                    next(request)
                else ->
                    intercept(next, request)
            }
        }

    protected abstract fun intercept(next: RequestTransformer, request: Request): Request
}

fun Request.noAuthentication() = request.apply {
    enabledFeatures[FEATURE] = this
}
