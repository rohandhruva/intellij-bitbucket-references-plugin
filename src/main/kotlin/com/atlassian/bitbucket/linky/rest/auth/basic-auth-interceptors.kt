package com.atlassian.bitbucket.linky.rest.auth

import com.atlassian.bitbucket.linky.rest.auth.Authentication.Basic
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.github.kittinunf.fuel.core.FoldableRequestInterceptor
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.RequestTransformer
import com.github.kittinunf.fuel.core.extensions.authentication

fun basicAuthInterceptor(cloud: BitbucketCloud, auth: Basic): FoldableRequestInterceptor =
    object : RequestAuthenticationInterceptorBase(cloud) {
        override fun intercept(next: RequestTransformer, request: Request): Request =
            next(request.authentication().basic(auth.username, auth.password))
    }

fun basicAuthInterceptor(server: BitbucketServer, auth: Basic): FoldableRequestInterceptor =
    object : RequestAuthenticationInterceptorBase(server) {
        override fun intercept(next: RequestTransformer, request: Request): Request =
            next(request.authentication().basic(auth.username, auth.password))
    }
