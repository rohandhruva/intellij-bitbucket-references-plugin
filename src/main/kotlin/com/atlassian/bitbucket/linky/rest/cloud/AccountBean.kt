package com.atlassian.bitbucket.linky.rest.cloud

data class AccountBean(
    val displayName: String? = null,
    val nickname: String? = null
)
