package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.auth.Authentication.*
import com.atlassian.bitbucket.linky.rest.auth.basicAuthInterceptor
import com.atlassian.bitbucket.linky.rest.cloud.config.defaultGson
import com.atlassian.bitbucket.linky.rest.cloud.oauth.BitbucketCloudOAuthApiImpl
import com.atlassian.bitbucket.linky.rest.cloud.oauth.OAuthInterceptors
import com.github.kittinunf.fuel.core.FuelManager
import com.google.gson.Gson

class BitbucketCloudApiImpl(
    private val createFuel: () -> FuelManager = { FuelManager() },
    private val gson: Gson = defaultGson(),
    customConfig: BitbucketCloudApiConfig.() -> Unit = { }
) : BitbucketCloudApi {

    private val config = BitbucketCloudApiConfig().apply(customConfig)
    private val fuel = createFuel()

    init {
        with(config) {
            when (val auth = authentication) {
                is Basic -> {
                    fuel.addRequestInterceptor(basicAuthInterceptor(instance, auth))
                }
                is OAuth -> {
                    val oAuthApi = BitbucketCloudOAuthApiImpl(createFuel, gson) {
                        instance = this@with.instance
                        consumer = auth.consumer
                    }
                    val interceptors = OAuthInterceptors(oAuthApi, instance, auth.tokenManager)
                    fuel.addRequestInterceptor(interceptors.requestInterceptor())
                    fuel.addResponseInterceptor(interceptors.responseInterceptor())
                }
                is AccessToken -> throw IllegalArgumentException("Bitbucket Cloud doesn't support access token authentication")
                else -> fuel
            }
        }
    }

    override fun repository(repositoryId: RepositoryId): RepositoryApi =
        RepositoryApiImpl(fuel, gson, config.instance, repositoryId)

    override fun snippets(workspaceId: String?): SnippetsApi = SnippetsApiImpl(fuel, gson, config.instance, workspaceId)
}
