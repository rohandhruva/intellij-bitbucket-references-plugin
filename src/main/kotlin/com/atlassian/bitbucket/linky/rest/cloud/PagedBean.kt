package com.atlassian.bitbucket.linky.rest.cloud

data class PagedBean<T>(
    val page: Int,
    val pagelen: Int,
    val size: Int? = null,
    val next: String? = null,
    val previous: String? = null,
    val values: List<T> = emptyList()
)
