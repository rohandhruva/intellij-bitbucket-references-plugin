package com.atlassian.bitbucket.linky.rest.cloud

import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.BlobDataPart
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.Headers
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.net.URI
import java.util.*
import java.util.concurrent.CompletableFuture

class SnippetsApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketCloud,
    private val workspaceId: String?
) : SnippetsApi {

    override fun create(
        files: List<SnippetFile>,
        title: String?,
        access: Access,
        scm: Scm
    ): CompletableFuture<Snippet> {
        val future = CompletableFuture<Snippet>()
        val createSnippetUrl = when (workspaceId) {
            null -> instance.createSnippetUrl
            else -> instance.createSnippetForUserUrl(workspaceId)
        }

        val parameters = mutableListOf<Pair<String, Any?>>(
            "scm" to scm.name.toLowerCase(Locale.ENGLISH),
            "is_private" to (access == Access.PRIVATE)
        )
        title?.let { parameters.add("title" to it) }

        fuel.upload(createSnippetUrl.toString(), parameters = parameters)
            .apply {
                for (file in files) {
                    add(
                        BlobDataPart(
                            file.contentProvider(),
                            "file",
                            filename = file.name
                        )
                    )
                }
            }
            .header(Headers.ACCEPT, "application/json")
            .responseObject<SnippetBean>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(it.toSnippet()) },
                    failure = { future.completeExceptionally(it.mapException("create snippet")) }
                )
            }

        return future
    }
}

private val query = "?fields=id,links.html"

private val BitbucketCloud.createSnippetUrl: URI
    get() = apiBaseUrl.resolve("2.0/snippets$query")

private fun BitbucketCloud.createSnippetForUserUrl(accountId: String): URI =
    apiBaseUrl.resolve("2.0/snippets/$accountId$query")

private data class SnippetBean(
    val id: String,
    val links: HtmlLinksBean
) {
    fun toSnippet() = Snippet(
        id = id,
        link = links.uri()
    )
}
