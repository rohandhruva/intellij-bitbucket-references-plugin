package com.atlassian.bitbucket.linky.rest.cloud

import java.io.InputStream
import java.net.URI
import java.util.concurrent.CompletableFuture

interface SnippetsApi {
    fun create(
        files: List<SnippetFile>,
        title: String? = null,
        access: Access = Access.PRIVATE,
        scm: Scm = Scm.GIT
    ): CompletableFuture<Snippet>
}

data class SnippetFile(val name: String, val contentProvider: () -> InputStream)

enum class Access { PRIVATE, PUBLIC }
enum class Scm { GIT, HG }

data class Snippet(
    val id: String,
    val link: URI
)
