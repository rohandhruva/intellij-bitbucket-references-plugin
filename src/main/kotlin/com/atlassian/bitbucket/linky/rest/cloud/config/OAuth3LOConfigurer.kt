package com.atlassian.bitbucket.linky.rest.cloud.config

import com.atlassian.bitbucket.linky.LinkyBundle.message
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.atlassian.bitbucket.linky.rest.cloud.oauth.AuthorizationCode
import com.atlassian.bitbucket.linky.rest.cloud.oauth.BitbucketCloudOAuthApiImpl
import com.atlassian.bitbucket.linky.rest.configureFuelHttpClient
import com.intellij.ide.BrowserUtil
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.MessageDialogBuilder
import com.intellij.openapi.ui.Messages
import java.util.*
import java.util.concurrent.CompletableFuture

interface OAuth3LOConfigurer {
    fun configureOAuth3LO(
        cloud: BitbucketCloud,
        actionDescription: String
    ): CompletableFuture<RefreshAndAccessTokens?>
}

class DefaultOAuth3LOConfigurer(private val project: Project) : OAuth3LOConfigurer {

    override fun configureOAuth3LO(
        cloud: BitbucketCloud,
        actionDescription: String
    ): CompletableFuture<RefreshAndAccessTokens?> {
        val future = CompletableFuture<RefreshAndAccessTokens?>()

        ApplicationManager.getApplication().invokeLater {
            if (userWantsToConfigureOAuth(actionDescription)) {
                getAuthorizationCode(cloud)
                    .thenApply { authCode ->
                        if (authCode != null) {
                            requestOAuthTokens(cloud, authCode)
                                .thenApply {
                                    future.complete(it)
                                }
                        }
                    }
            } else {
                future.complete(null)
            }
        }
        return future
    }

    private fun getAuthorizationCode(cloud: BitbucketCloud): CompletableFuture<String?> {
        val codeFuture = CompletableFuture<String?>()
        val stateToken = UUID.randomUUID().toString()
        val callback = object : OAuth3LOCallback {
            override val id = stateToken
            override fun onSuccess(code: String) {
                codeFuture.complete(code)
            }

            override fun onDecline() {
                codeFuture.complete(null)
            }

            override fun onError(description: String?) {
                // TODO show error flag
                codeFuture.complete(null)
            }
        }
        OAuth3LOCallbackService.expectCallback(callback)
        BrowserUtil.browse(cloud.oAuthAuthorizationUrl(stateToken))

        return codeFuture
    }

    private fun requestOAuthTokens(cloud: BitbucketCloud, authCode: String): CompletableFuture<RefreshAndAccessTokens> {
        val oAuthApi = BitbucketCloudOAuthApiImpl(
            createFuel = { configureFuelHttpClient(cloud.baseUrl) }
        ) {
            instance = cloud
            consumer = cloud.oAuthConsumer
        }
        return oAuthApi.requestOAuthTokens(AuthorizationCode(authCode))
    }

    private fun userWantsToConfigureOAuth(actionDescription: String): Boolean =
        Messages.YES == MessageDialogBuilder
            .yesNo(
                message("authentication.required.title"),
                message("oauth.authentication.required.text", actionDescription)
            )
            .yesText(message("oauth.authentication.required.configure.button.label"))
            .noText(message("authentication.required.cancel.button.label"))
            .project(project)
            .show()
}
