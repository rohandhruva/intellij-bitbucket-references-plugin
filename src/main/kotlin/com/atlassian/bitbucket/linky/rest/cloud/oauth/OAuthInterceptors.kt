package com.atlassian.bitbucket.linky.rest.cloud.oauth

import com.atlassian.bitbucket.linky.rest.auth.OAuthTokenManager
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshAndAccessTokens
import com.atlassian.bitbucket.linky.rest.auth.OAuthTokens.RefreshTokenOnly
import com.atlassian.bitbucket.linky.rest.auth.RefreshToken
import com.atlassian.bitbucket.linky.rest.auth.RequestAuthenticationInterceptorBase
import com.atlassian.bitbucket.linky.rest.cloud.BitbucketCloud
import com.github.kittinunf.fuel.core.*
import com.github.kittinunf.fuel.core.extensions.authentication
import com.github.kittinunf.fuel.core.requests.RepeatableBody
import java.util.concurrent.ExecutionException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

private const val bearerPrefix = "Bearer "
private const val unauthorizedStatus = 401

class OAuthInterceptors(
    private val oAuthApi: BitbucketCloudOAuthApi,
    private val cloud: BitbucketCloud,
    private val tokenManager: OAuthTokenManager
) {
    fun requestInterceptor(): FoldableRequestInterceptor =
        object : RequestAuthenticationInterceptorBase(cloud) {
            override fun intercept(next: RequestTransformer, request: Request): Request {
                val tokens = tokensForCall()
                request.authentication().bearer(tokens.accessToken.value)
                return next(request)
            }
        }

    fun responseInterceptor(): FoldableResponseInterceptor =
        { next: ResponseTransformer ->
            inner@{ request: Request, response: Response ->
                // Only react on 401 Unauthorized
                if (response.statusCode != unauthorizedStatus) {
                    return@inner next(request, response)
                }

                if (!request.body.isEmpty() && request.body.isConsumed() && request.body !is RepeatableBody) {
                    // We can't send the body again. This request can potentially be retried
                    // by the caller, so we throw an exception indicating that
                    throw OAuthAccessTokenLateRefreshException()
                }

                // Parse the access token used for the request
                val requestAccessToken = request
                    .header(Headers.AUTHORIZATION)
                    .singleOrNull()
                    ?.bearerToken()
                    ?: return@inner next(request, response)

                // Try to refresh access token to either get the new one or raise an error indicating
                // that OAuth is not configured (in case the token was revoked on remote)
                val tokens = tokensForCall(requestAccessToken)

                request.authentication().bearer(tokens.accessToken.value)

                next(request, request.response().second)
            }
        }

    private fun tokensForCall(previousCallAccessToken: String? = null): RefreshAndAccessTokens =
        when (val currentTokens = tokenManager.getOAuthTokens()) {
            is RefreshTokenOnly -> refreshTokens(currentTokens.refreshToken)
            is RefreshAndAccessTokens -> {
                val accessToken = currentTokens.accessToken
                // Refresh tokens only if they haven't been already refreshed
                if (previousCallAccessToken == accessToken.value || accessToken.isExpired()) {
                    refreshTokens(currentTokens.refreshToken)
                } else {
                    currentTokens
                }
            }
        }

    private fun refreshTokens(refreshToken: RefreshToken): RefreshAndAccessTokens =
        try {
            oAuthApi.refreshOAuthTokens(refreshToken)
                .get(10, TimeUnit.SECONDS)
                .also { tokenManager.saveOAuthTokens(it) }
        } catch (e: ExecutionException) {
            throw OAuthException("Error when refreshing OAuth access code", e)
        } catch (e: TimeoutException) {
            throw OAuthException("Timeout when refreshing OAuth access code", e)
        }
}


private fun String.bearerToken() =
    takeIf { it.startsWith(bearerPrefix) }?.substringAfter(bearerPrefix)

/**
 * Parent class for all OAuth-related exceptions.
 */
open class OAuthException(message: String, cause: Throwable? = null) : RuntimeException(message, cause)

// TODO never thrown
/**
 * Bitbucket rejected provided OAuth refresh token, configuration change is required.
 */
class OAuthNotConfiguredException : OAuthException("OAuth is not configured")

/**
 * Bitbucket responded with 401 (Unauthorized) to a request which was authenticated
 * with an access token that was supposed to be valid. It has been refreshed after
 * the failed request, but that request can't be retried automatically as it had
 * a non-empty body. The caller might be able to retry the request.
 */
class OAuthAccessTokenLateRefreshException :
    OAuthException("OAuth access token happened to be out of date, request can't be retried automatically")
