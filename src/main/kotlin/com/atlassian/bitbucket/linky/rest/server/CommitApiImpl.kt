package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.PullRequest
import com.atlassian.bitbucket.linky.rest.mapException
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.gson.responseObject
import com.google.gson.Gson
import java.net.URI
import java.time.ZonedDateTime
import java.util.concurrent.CompletableFuture

class CommitApiImpl(
    private val fuel: FuelManager,
    private val gson: Gson,
    private val instance: BitbucketServer,
    private val repositoryId: RepositoryId,
    private val commit: String
) : CommitApi {

    override fun relatedPullRequests(start: Int): CompletableFuture<ServerPage<PullRequest>> {
        val future = CompletableFuture<ServerPage<PullRequest>>()

        fuel.get(instance.relatedPullRequestsUrl(repositoryId, commit, start).toString())
            .responseObject<PagedBean<PullRequestBean>>(gson) { _, _, result ->
                result.fold(
                    success = { future.complete(it.toPullRequestsPage()) },
                    failure = { future.completeExceptionally(it.mapException("commit pull requests")) }
                )
            }
        return future
    }

    private fun PagedBean<PullRequestBean>.toPullRequestsPage(): ServerPage<PullRequest> {
        fun pageFetcher(start: Int) = { api: BitbucketServerApi ->
            api.repository(repositoryId)
                .commit(commit)
                .relatedPullRequests(start)
        }
        return ServerPage(
            items = values.map { it.toPullRequest() },
            pageSize = limit,
            totalSize = size,
            nextPage = nextPageStart?.let { pageFetcher(nextPageStart) }
        )
    }
}

private fun BitbucketServer.relatedPullRequestsUrl(
    repositoryId: RepositoryId,
    commitHash: String,
    start: Int
): URI =
    apiBaseUrl.resolve("rest/api/1.0/${repositoryId.urlPresentation}/commits/$commitHash/pull-requests?start=$start")


private data class PullRequestBean(
    val id: Long,
    val title: String,
    val description: String?,
    val fromRef: RefInfoBean,
    val toRef: RefInfoBean,
    val state: PullRequestState,
    val author: AuthorBean,
    val createdDate: ZonedDateTime,
    val links: SelfLinksBean
) {
    fun toPullRequest() = PullRequest(
        id = id,
        title = title,
        description = description,
        sourceBranchName = fromRef.displayId,
        destinationBranchName = toRef.displayId,
        state = state.name,
        authorName = author.user.displayName,
        createdDate = createdDate,
        link = links.uri()
    )
}

private data class AuthorBean(val user: UserBean)

private data class RefInfoBean(
    val id: String,
    val displayId: String,
    val latestCommit: String
)
