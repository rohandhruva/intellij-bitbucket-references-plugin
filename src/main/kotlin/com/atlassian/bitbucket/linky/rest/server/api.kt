package com.atlassian.bitbucket.linky.rest.server

import com.atlassian.bitbucket.linky.rest.auth.Authentication
import com.atlassian.bitbucket.linky.rest.auth.Authentication.Guest
import com.atlassian.bitbucket.linky.rest.pagination.Page
import java.net.URI

interface BitbucketServerApi {
    fun personalAccessTokens(username: String): PersonalAccessTokensApi
    fun repository(repositoryId: RepositoryId): RepositoryApi
    fun testConnectivity(): ConnectivityTestApi
}

typealias ServerPage<T> = Page<BitbucketServerApi, T>

interface RepositoryApi {
    fun commit(commit: String): CommitApi
}

class BitbucketServerApiConfig {
    lateinit var instance: BitbucketServer
    var authentication: Authentication = Guest
}

data class BitbucketServer(val baseUrl: URI) {
    val hostname: String = baseUrl.host
    val apiBaseUrl: URI = baseUrl

    init {
        if (!baseUrl.path.endsWith('/')) {
            throw IllegalArgumentException("The URL path should end with a slash")
        }
    }
}

data class RepositoryId(val project: String, val slug: String) {
    val urlPresentation = "projects/$project/repos/$slug"
    val displayPresentation = "$project/$slug"
}
