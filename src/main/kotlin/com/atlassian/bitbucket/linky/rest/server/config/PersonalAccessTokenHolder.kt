package com.atlassian.bitbucket.linky.rest.server.config

import com.atlassian.bitbucket.linky.generateServiceName
import com.atlassian.bitbucket.linky.hosting.uuid
import com.atlassian.bitbucket.linky.rest.auth.PersonalAccessToken
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.intellij.credentialStore.CredentialAttributes
import com.intellij.credentialStore.Credentials
import com.intellij.ide.passwordSafe.PasswordSafe
import com.intellij.openapi.components.service

interface PersonalAccessTokenHolder {
    fun getPersonalAccessToken(instance: BitbucketServer): PersonalAccessToken?
    fun savePersonalAccessToken(instance: BitbucketServer, personalAccessToken: PersonalAccessToken)
}

class DefaultPersonalAccessTokenHolder : PersonalAccessTokenHolder {

    override fun getPersonalAccessToken(instance: BitbucketServer): PersonalAccessToken? =
        service<PasswordSafe>().getPassword(credentialsAttributes(instance))
            ?.let { PersonalAccessToken(it) }

    override fun savePersonalAccessToken(instance: BitbucketServer, personalAccessToken: PersonalAccessToken) {
        service<PasswordSafe>().set(
            credentialsAttributes(instance),
            Credentials(instance.uuid.toString(), personalAccessToken.value)
        )
    }

    private fun credentialsAttributes(server: BitbucketServer): CredentialAttributes {
        val name = server.uuid.toString()
        return CredentialAttributes(generateServiceName("Bitbucket Server $name"), name, null, false)
    }
}
