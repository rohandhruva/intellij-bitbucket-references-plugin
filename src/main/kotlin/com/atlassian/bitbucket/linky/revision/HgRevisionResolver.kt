package com.atlassian.bitbucket.linky.revision

import com.atlassian.bitbucket.linky.Revision
import com.intellij.openapi.vcs.history.VcsRevisionNumber
import org.zmlx.hg4idea.HgRevisionNumber

class HgRevisionResolver : VcsRevisionResolver {
    override fun resolveVcsRevision(vcsRevisionNumber: VcsRevisionNumber): Revision? =
        when (vcsRevisionNumber) {
            is HgRevisionNumber -> vcsRevisionNumber.changeset
            else -> null
        }
}
