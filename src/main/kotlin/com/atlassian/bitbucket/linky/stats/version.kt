package com.atlassian.bitbucket.linky.stats

val linkyVersion: String = object : Any() {}.javaClass.classLoader
    .getResourceAsStream("/META-INF/plugin.xml")
    ?.bufferedReader()
    ?.lines()
    ?.map { it.trim() }
    ?.filter { it.startsWith("<version>") && it.endsWith("</version>") }
    ?.findFirst()
    ?.map { it.substring(9, it.length - 10) }
    ?.orElse("unknown")
    ?: "failed"
