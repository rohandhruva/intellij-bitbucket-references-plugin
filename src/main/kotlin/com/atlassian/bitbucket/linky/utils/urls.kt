package com.atlassian.bitbucket.linky.utils

fun String.appendTrailSlash() = trimEnd('/') + '/'
