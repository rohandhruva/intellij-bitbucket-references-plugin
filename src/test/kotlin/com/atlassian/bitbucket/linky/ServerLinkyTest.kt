package com.atlassian.bitbucket.linky

import assertk.assertThat
import assertk.assertions.hasToString
import assertk.assertions.isFalse
import assertk.assertions.isTrue
import com.atlassian.bitbucket.linky.blame.LineBlamer
import com.atlassian.bitbucket.linky.discovery.RemoteUrl
import com.atlassian.bitbucket.linky.preferences.Preferences
import com.atlassian.bitbucket.linky.preferences.preferences
import com.atlassian.bitbucket.linky.rest.server.BitbucketServer
import com.atlassian.bitbucket.linky.selection.LinesSelection
import com.intellij.dvcs.repo.Repository
import com.intellij.mock.MockVirtualFile
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.net.URI
import java.net.URLEncoder

internal class ServerLinkyTest {

    private val lineBlamer = mockk<LineBlamer>()
    private val repository = mockk<Repository>()
    private val preferences = mockk<Preferences>()

    private lateinit var linkyFile: LinkyFile
    private lateinit var linky: ServerLinky

    @BeforeEach
    internal fun setUp() {
        val virtualFile = MockVirtualFile("file")
        linkyFile = LinkyFile(virtualFile, repository, "someRevision", "dir/file", lineBlamer)

        val remoteUrl = RemoteUrl(UriScheme.HTTP, "example.com", 80, "/")

        val hosting = BitbucketServer(URI.create("http://example.com:1234/path/"))
        val serverRepo = BitbucketRepository.Server(
            repository,
            remoteUrl,
            hosting,
            "someProject",
            "someSlug"
        )
        linky = ServerLinky(serverRepo)

        every { lineBlamer.blameLine(linkyFile, 239) } returns null

        mockkStatic("com.atlassian.bitbucket.linky.preferences.RepositoryPreferencesKt")
        every { repository.preferences() } returns preferences
        every { preferences.getProperty(any(), any()) } returns null
    }

    @Test
    fun `test getCommitViewUri with lines selected`() {
        every { lineBlamer.blameLine(linkyFile, 239) } returns Pair("some/File", 405)

        val commitUri = linky.getCommitViewUri("sha", linkyFile, 239)

        assertThat(commitUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/commits/sha#some/File")
    }

    @Test
    fun `test getCommitViewUri with SHA reference`() {
        val commitUri = linky.getCommitViewUri("abcdef", linkyFile, 239)

        assertThat(commitUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/commits/abcdef")
    }

    @Test
    fun `test getSourceViewUri`() {
        val sourceUri = linky.getSourceViewUri()

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse")
    }

    @Test
    fun `test getSourceViewUri for file`() {
        val sourceUri = linky.getSourceViewUri(linkyFile, listOf())

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir/file?at=someRevision")
    }

    @Test
    fun `test getSourceViewUri for file with lines selected`() {
        val sourceUri = linky.getSourceViewUri(linkyFile, listOf(LinesSelection(1, 7), LinesSelection(239, 405)))

        assertThat(sourceUri).hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/browse/dir/file?at=someRevision#1-7,239-405")
    }

    @Test
    fun `test getPullRequestUri when branch absent`() {
        every { repository.currentBranchName } returns null

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isFalse()
    }

    @Test
    fun `test getPullRequestUri when branch present`() {
        every { repository.currentBranchName } returns "someBranch"

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isTrue()
        assertThat(maybePullRequestUri.get())
            .hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/pull-requests?create&sourceBranch=someBranch")
    }

    @Test
    fun `test getPullRequestUri branch name escaped`() {
        val branchName = "branch#5><|.hello!&])({}@.@$&+_-`'\""
        every { repository.currentBranchName } returns branchName
        val encodedBranch = URLEncoder.encode(branchName, Charsets.UTF_8.name())

        val maybePullRequestUri = linky.getPullRequestUri()

        assertThat(maybePullRequestUri.isPresent).isTrue()
        assertThat(maybePullRequestUri.get())
            .hasToString("http://example.com:1234/path/projects/someProject/repos/someSlug/pull-requests?create&sourceBranch=" + encodedBranch)
    }

}
