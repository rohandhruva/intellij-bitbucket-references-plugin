//package com.atlassian.bitbucket.linky.discovery
//
//import assertk.all
//import assertk.assertThat
//import assertk.assertions.*
//import com.atlassian.bitbucket.linky.BitbucketRepository
//import com.atlassian.bitbucket.linky.UriScheme.HTTPS
//import com.atlassian.bitbucket.linky.hosting.BitbucketCloudRegistry
//import com.intellij.dvcs.repo.Repository
//import io.mockk.called
//import io.mockk.every
//import io.mockk.mockk
//import io.mockk.verify
//import kotlinx.coroutines.runBlocking
//import org.junit.jupiter.api.BeforeEach
//import org.junit.jupiter.api.Test
//
//internal class CloudBitbucketRepositoryDiscovererTest {
//    private val registry = mockk<BitbucketCloudRegistry>(relaxUnitFun = true)
//    private val repository = mockk<Repository>()
//    private val probe = mockk<BitbucketCloudProbe>()
//
//    private val discoverer = CloudBitbucketRepositoryDiscoverer(registry, probe)
//
//    private val remoteUrl = RemoteUrl(HTTPS, "bitbucket.org", -1, "/bitbucket/test")
//
//    @BeforeEach
//    internal fun setUp() {
//        every { registry.lookup(any(), any(), any()) } returns null
//        every { probe.probeBitbucketCloud(any(), any(), any()) } returns BitbucketCloud.Production
//    }
//
//    @Test
//    fun `test repository discovery`() {
//        val repo = runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        assertThat(repo).isNotNull().isInstanceOf(BitbucketRepository.Cloud::class).all {
//            prop(BitbucketRepository.Cloud::repository).isEqualTo(repository)
//            prop(BitbucketRepository.Cloud::hosting).isEqualTo(BitbucketCloud.Production)
//            prop(BitbucketRepository.Cloud::workspaceId).isEqualTo("bitbucket")
//            prop(BitbucketRepository.Cloud::slug).isEqualTo("test")
//        }
//    }
//
//    @Test
//    fun `should not probe if hosting found in registry`() {
//        every { registry.lookup(any(), any(), any()) } returns BitbucketCloud.Production
//
//        runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        verify { registry.lookup(HTTPS, "bitbucket.org", -1) }
//        verify(exactly = 0) { registry.add(any(), any(), any(), any()) }
//        verify { probe wasNot called }
//    }
//
//    @Test
//    fun `should not probe if not active discovery`() {
//        val repository = runBlocking { discoverer.discover(repository, remoteUrl, false) }
//
//        assertThat(repository).isNull()
//
//        verify { registry.lookup(HTTPS, "bitbucket.org", -1) }
//        verify(exactly = 0) { registry.add(any(), any(), any(), any()) }
//        verify { probe wasNot called }
//    }
//
//    @Test
//    fun `should not register if probe found nothing`() {
//        every { probe.probeBitbucketCloud(any(), any(), any()) } returns null
//
//        val repository = runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        assertThat(repository).isNull()
//
//        verify { registry.lookup(HTTPS, "bitbucket.org", -1) }
//        verify(exactly = 0) { registry.add(any(), any(), any(), any()) }
//        verify { probe.probeBitbucketCloud(HTTPS, "bitbucket.org", -1) }
//    }
//
//    @Test
//    fun `should register if probe found Bitbucket`() {
//        runBlocking { discoverer.discover(repository, remoteUrl, true) }
//
//        verify { registry.lookup(HTTPS, "bitbucket.org", -1) }
//        verify { registry.add(any(), any(), any(), any()) }
//        verify { probe.probeBitbucketCloud(HTTPS, "bitbucket.org", -1) }
//    }
//
//    @Test
//    fun `should skip remotes if path unknown`() {
//        val discoverRepoForRemoteWithPath = { path: String ->
//            runBlocking {
//                discoverer.discover(repository, RemoteUrl(HTTPS, "example.com", -1, path), true)
//            }
//        }
//
//        assertThat(discoverRepoForRemoteWithPath("/three/segments/path")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("/short_path")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("/a//b")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("///a")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("//a")).isNull()
//        assertThat(discoverRepoForRemoteWithPath("/")).isNull()
//
//        verify { registry wasNot called }
//        verify { probe wasNot called }
//    }
//}
